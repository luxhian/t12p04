package t12p04;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.Alumno;
import modelo.ConexionBD;
import modelo.Curso;

public class panelBuscadorAlumno extends javax.swing.JPanel implements IBusCallBack {

    private ConexionBD bd;
    DefaultTableModel listado;
    private panelAltaAlumno panelAA;
    

    public panelBuscadorAlumno(ConexionBD bd, panelAltaAlumno panelAA) {
        initComponents();
        this.bd = bd;
        this.panelAA = panelAA;
        listado = new DefaultTableModel();
        listado.setColumnIdentifiers(new Object[] {"Curso","DNI","Nombre","Mayor Edad"});
        table.setModel(listado);
    }

    public void mostrar() {
        setVisible(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txtCurso = new javax.swing.JTextField();
        txtDNI = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        but_Buscar = new javax.swing.JButton();
        but_Limpiar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        but_Alta = new javax.swing.JButton();
        but_Baja = new javax.swing.JButton();
        but_Editar = new javax.swing.JButton();
        but_Aceptar = new javax.swing.JButton();
        check_valorExacto = new javax.swing.JCheckBox();

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("BUSCADOR DE ALUMNOS");

        jLabel2.setText("Curso");

        jLabel3.setText("DNI");

        but_Buscar.setText("Buscar");
        but_Buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                but_BuscarActionPerformed(evt);
            }
        });

        but_Limpiar.setText("Limpiar");
        but_Limpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                but_LimpiarActionPerformed(evt);
            }
        });

        table.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(table);

        but_Alta.setText("Alta");
        but_Alta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                but_AltaActionPerformed(evt);
            }
        });

        but_Baja.setText("Baja");
        but_Baja.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                but_BajaActionPerformed(evt);
            }
        });

        but_Editar.setText("Editar");
        but_Editar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                but_EditarActionPerformed(evt);
            }
        });

        but_Aceptar.setText("Aceptar");
        but_Aceptar.setMaximumSize(new java.awt.Dimension(75, 23));
        but_Aceptar.setMinimumSize(new java.awt.Dimension(75, 23));
        but_Aceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                but_AceptarActionPerformed(evt);
            }
        });

        check_valorExacto.setText("Buscar valor exacto");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addComponent(but_Alta, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(but_Baja, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(but_Editar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(but_Aceptar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtCurso, javax.swing.GroupLayout.PREFERRED_SIZE, 305, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(but_Buscar, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(but_Limpiar, javax.swing.GroupLayout.Alignment.TRAILING)))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(check_valorExacto)
                                    .addComponent(txtDNI, javax.swing.GroupLayout.PREFERRED_SIZE, 305, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 520, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(152, 152, 152))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtCurso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(but_Buscar)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(but_Limpiar)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3)
                        .addComponent(txtDNI, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(check_valorExacto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(but_Alta)
                    .addComponent(but_Baja)
                    .addComponent(but_Editar)
                    .addComponent(but_Aceptar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void but_AceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_but_AceptarActionPerformed
        setVisible(false);
    }//GEN-LAST:event_but_AceptarActionPerformed

    private void but_BuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_but_BuscarActionPerformed
        listado.setRowCount(0);
        List<Alumno> TAlumnos = new ArrayList<>();

        try {
            Alumno.listadoFiltrado(bd, TAlumnos, txtCurso.getText(),
                    txtDNI.getText(), check_valorExacto.isSelected());
            Collections.sort(TAlumnos);
            for (Alumno a : TAlumnos) {
                String[] s={String.valueOf(a.getIdCurso()),a.getDni()
                            ,a.getNombre(),String.valueOf(a.isMayorEdad())};
                listado.addRow(s);
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Error!!\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            setVisible(false);
            return;
        }
    }//GEN-LAST:event_but_BuscarActionPerformed

    private void but_LimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_but_LimpiarActionPerformed
        listado.setRowCount(0);
        txtCurso.setText("");
        txtDNI.setText("");
        check_valorExacto.setSelected(false);
    }//GEN-LAST:event_but_LimpiarActionPerformed

    private void but_AltaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_but_AltaActionPerformed
        panelAA.mostrar(this);
    }//GEN-LAST:event_but_AltaActionPerformed

    private void but_BajaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_but_BajaActionPerformed
        try {
            if (table.getSelectedRowCount() == 1) {
                Alumno a = new Alumno();
                a.setIdCurso(Integer.valueOf((String) listado.getValueAt(table.getSelectedRow(), 0)));
                a.setDni((String) listado.getValueAt(table.getSelectedRow(), 1));
                int op = JOptionPane.showConfirmDialog(this,
                        "¿Está seguro de que desea dar de baja al alumno?",
                        "Borrar",
                        JOptionPane.YES_NO_OPTION);
                switch (op) {
                    case JOptionPane.YES_OPTION:
                        a.bajaAlumno(bd);
                        JOptionPane.showMessageDialog(this, "Baja de Alumno correcta!!",
                                "Baja Alumno", JOptionPane.INFORMATION_MESSAGE);
                        actualizarBuscador();
                        break;
                }
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Error!!\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }


    }//GEN-LAST:event_but_BajaActionPerformed

    private void but_EditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_but_EditarActionPerformed
        try {
            Alumno a= new Alumno();
            a.setIdCurso(Integer.valueOf((String) listado.getValueAt(table.getSelectedRow(), 0)));
            a.setDni((String) listado.getValueAt(table.getSelectedRow(), 1));
            a.setNombre((String) listado.getValueAt(table.getSelectedRow(), 2));
            if (((String) listado.getValueAt(table.getSelectedRow(), 3)).equals("true")){
                a.setMayorEdad(true);
            } else if (((String) listado.getValueAt(table.getSelectedRow(), 3)).equals("false")){
                a.setMayorEdad(false);
            }
            
            panelAA.mostrar(a, this);


        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Error!!\n" + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_but_EditarActionPerformed

    @Override
    public void actualizarBuscador() {
        but_Buscar.doClick();
        setVisible(true);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton but_Aceptar;
    private javax.swing.JButton but_Alta;
    private javax.swing.JButton but_Baja;
    private javax.swing.JButton but_Buscar;
    private javax.swing.JButton but_Editar;
    private javax.swing.JButton but_Limpiar;
    private javax.swing.JCheckBox check_valorExacto;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable table;
    private javax.swing.JTextField txtCurso;
    private javax.swing.JTextField txtDNI;
    // End of variables declaration//GEN-END:variables
}

package modelo;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class Curso implements Comparable<Curso> {

    /* Atributos **************************************************************/
    public enum ESTADO {
        PROGRAMADO, REALIZANDOSE, FINALIZADO
    }

    private int id;
    private String titulo;
    private double horas;
    private LocalDate fecIni;
    private LocalDate fecFin;
    private char modalidad;
    private ESTADO estado;

    /* Constructores **********************************************************/
    public Curso() {
        id = 0;
        titulo = "";
        horas = 0.0;
        fecIni = null;
        fecFin = null;
        modalidad = ' ';
        estado = null;
    }

    /* Métodos getters & setters **********************************************/
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public double getHoras() {
        return horas;
    }

    public void setHoras(double horas) {
        this.horas = horas;
    }

    public String getFecIni() {
        if (fecIni != null) {
            return fecIni.format(DateTimeFormatter.ofPattern("dd/MM/uuuu"));
        } else {
            return "";
        }
    }

    public void setFecIni(String fecIni) {
        this.fecIni = LocalDate.of(Integer.parseInt(fecIni.substring(6, 10)),
                Integer.parseInt(fecIni.substring(3, 5)),
                Integer.parseInt(fecIni.substring(0, 2)));
    }

    public String getFecFin() {
        if (fecFin != null) {
            return fecFin.format(DateTimeFormatter.ofPattern("dd/MM/uuuu"));
        } else {
            return "";
        }
    }

    public void setFecFin(String fecFin) {
        this.fecFin = LocalDate.of(Integer.parseInt(fecFin.substring(6, 10)),
                Integer.parseInt(fecFin.substring(3, 5)),
                Integer.parseInt(fecFin.substring(0, 2)));
    }

    public char getModalidad() {
        return modalidad;
    }

    public void setModalidad(char modalidad) {
        this.modalidad = modalidad;
    }

    public String getEstado() {
        if (estado != null) {
            return estado.toString();
        } else {
            return "";
        }
    }

    public void setEstado(ESTADO estado) {
        this.estado = estado;
    }

    public void setEstado(String estado) {
        switch (estado.charAt(0)) {
            case 'P':
                this.estado=ESTADO.PROGRAMADO;
                break;
            case 'R':
                this.estado=ESTADO.REALIZANDOSE;
                break;
            case 'F':
                this.estado=ESTADO.FINALIZADO;
                break;
            default:
                break;
        }
    }
    
    public void setFecIni(LocalDate fecIni) {
        this.fecIni = fecIni;
    }

    public void setFecFin(LocalDate fecFin) {
        this.fecFin = fecFin;
    }

    /* Métodos ****************************************************************/
    public boolean existeCurso(ConexionBD bd) throws Exception {
        try {
            String sql = "SELECT count(*) FROM Cursos WHERE "
                    + "id=" + id;
            ResultSet rs = bd.getSt().executeQuery(sql);
            rs.next();
            int n = rs.getInt(1);
            if (n > 0) {
                return true;
            }
        } catch (SQLException e) {
            throw new Exception("Error existeCurso()!!", e);
        }
        return false;
    }

    public void altaCurso(ConexionBD bd) throws Exception {
        if (existeCurso(bd)) {
            throw new Exception("El curso ya existe!!");
        }
        try {
            String sql = "INSERT INTO Cursos VALUES ("
                    + id + ",'"
                    + titulo + "',"
                    + horas + ","
                    + ((fecIni == null) ? null : "'" + fecIni + "'") + ","
                    + ((fecFin == null) ? null : "'" + fecFin + "'") + ",'"
                    + modalidad + "',"
                    + ((estado == null) ? null : "'" + estado + "'") + ")";
            bd.getSt().executeUpdate(sql);
        } catch (SQLException e) {
            throw new Exception("Error altaCurso()!!", e);
        }
    }

    public void bajaCurso(ConexionBD bd) throws Exception {
        if (!existeCurso(bd)) {
            throw new Exception("El curso no existe!!");
        }
        try {
            String sql = "DELETE FROM Cursos WHERE "
                    + "id=" + id;
            bd.getSt().executeUpdate(sql);
        } catch (SQLException e) {
            throw new Exception("Error bajaCurso()!!", e);
        }
    }

    public void bajaCursoSinCascade(ConexionBD bd) throws Exception {
        if (!existeCurso(bd)) {
            throw new Exception("El curso no existe!!");
        }
        try {
            bd.getConn().setAutoCommit(false);
            String sql = "DELETE FROM Alumnos WHERE "
                    + "idCurso=" + id;
            bd.getSt().executeUpdate(sql);
            sql = "DELETE FROM Cursos WHERE "
                    + "id=" + id;
            bd.getSt().executeUpdate(sql);
            bd.getConn().commit();
        } catch (SQLException e) {
            bd.getConn().rollback();
            throw new Exception("Error bajaCurso()!!", e);
        } finally {
            bd.getConn().setAutoCommit(true);
        }
    }

    public static void listadoCursos(ConexionBD bd, List<Curso> t) throws Exception {
        try {
            String sql = "SELECT * FROM Cursos";
            ResultSet rs = bd.getSt().executeQuery(sql);
            Curso c;
            while (rs.next()) {
                c = new Curso();
                c.setId(rs.getInt("id"));
                c.setTitulo(rs.getString("titulo"));
                c.setHoras(rs.getDouble("horas"));
                Date fechaI = rs.getDate("fecIni");
                Date fechaF = rs.getDate("fecFin");
                if (fechaI != null) {
                    c.setFecIni(fechaI.toLocalDate());
                }
                if (fechaF != null) {
                    c.setFecFin(fechaF.toLocalDate());
                }
                if (rs.getString("modalidad").charAt(0) == 'P') {
                    c.setModalidad('P');
                } else {
                    c.setModalidad('T');
                }

                if (rs.getString("estado") != null) {
                    switch (rs.getString("estado").charAt(0)) {
                        case 'P':
                            c.setEstado(ESTADO.PROGRAMADO);
                            break;
                        case 'R':
                            c.setEstado(ESTADO.REALIZANDOSE);
                            break;
                        case 'F':
                            c.setEstado(ESTADO.FINALIZADO);
                            break;
                        default:
                            break;
                    }
                }

                t.add(c);
            }
        } catch (SQLException e) {
            throw new Exception("Error listadoCursos()!!", e);
        }
    }

    public static void listadoFiltrado(ConexionBD bd, List<Curso> t, String id,
            String titulo, boolean valor) throws Exception {
        try {
            if (id.equals("") && titulo.equals("")) {
                String sql = "SELECT * FROM Cursos";
                listar(bd, sql, t);
            } else if (!id.equals("") && !titulo.equals("") && !valor) {
                String sql = "SELECT * FROM Cursos WHERE titulo like '" + titulo + "'"
                        + " AND id= " + Integer.valueOf(id) + "";
                listar(bd, sql, t);
            } else if (!id.equals("") && !titulo.equals("") && valor) {
                String sql = "SELECT * FROM Cursos WHERE titulo like '" + titulo + "%'"
                        + " AND id=" + Integer.valueOf(id) + "%";
                listar(bd, sql, t);
            } else if (id.equals("") && !titulo.equals("") && !valor) {
                String sql = "SELECT * FROM Cursos WHERE titulo like '" + titulo + "%'";
                listar(bd, sql, t);
            } else if (id.equals("") && !titulo.equals("") && valor) {
                String sql = "SELECT * FROM Cursos WHERE titulo like '" + titulo + "'";
                listar(bd, sql, t);
            } else if (!id.equals("") && titulo.equals("")) {
                String sql = "SELECT * FROM Cursos WHERE id =" + Integer.valueOf(id) + "";
                listar(bd, sql, t);
            }

        } catch (SQLException e) {
            throw new Exception("Error listadoFiltrado()!!", e);
        }
    }

    private static void listar(ConexionBD bd, String sql, List<Curso> t) throws Exception {
        try {
            ResultSet rs = bd.getSt().executeQuery(sql);
            Curso c;
            while (rs.next()) {
                c = new Curso();
                c.setId(rs.getInt("id"));
                c.setTitulo(rs.getString("titulo"));
                c.setHoras(rs.getDouble("horas"));
                Date fechaI = rs.getDate("fecIni");
                Date fechaF = rs.getDate("fecFin");
                if (fechaI != null) {
                    c.setFecIni(fechaI.toLocalDate());
                }
                if (fechaF != null) {
                    c.setFecFin(fechaF.toLocalDate());
                }
                if (rs.getString("modalidad").charAt(0) == 'P') {
                    c.setModalidad('P');
                } else {
                    c.setModalidad('T');
                }

                if (rs.getString("estado") != null) {
                    switch (rs.getString("estado").charAt(0)) {
                        case 'P':
                            c.setEstado(ESTADO.PROGRAMADO);
                            break;
                        case 'R':
                            c.setEstado(ESTADO.REALIZANDOSE);
                            break;
                        case 'F':
                            c.setEstado(ESTADO.FINALIZADO);
                            break;
                        default:
                            break;
                    }
                }

                t.add(c);
            }
        } catch (SQLException e) {
            throw new Exception("Error listar()!!", e);
        }
    }
    
    public void editarCurso(ConexionBD bd) throws Exception {
        if (!existeCurso(bd)) {
            throw new Exception("El curso no existe!!");
        }
        try {
            String sql = "UPDATE Cursos SET titulo='"+titulo+"',"
                    + " horas="+horas+","
                    + "fecIni="+ ((fecIni == null) ? null : "'" + fecIni + "'") + ","
                    + "fecFin="+ ((fecFin == null) ? null : "'" + fecFin + "'") + ","
                    + "modalidad='"+modalidad+"',"
                    + "estado="+ ((estado == null) ? null : "'" + estado + "'")
                    + " WHERE id=" + id;
            bd.getSt().executeUpdate(sql);
        } catch (SQLException e) {
            throw new Exception("Error bajaCurso()!!", e);
        }
    }
    
    public Curso buscaCurso(ConexionBD bd) throws Exception{
        if (!existeCurso(bd)) {
            throw new Exception("El curso no existe!!");
        }
        try {
            Curso c=new Curso();
            String sql="SELECT titulo FROM Cursos WHERE id="+id;
            ResultSet rs=bd.getSt().executeQuery(sql);
            rs.next();
            c.setTitulo(rs.getString("titulo"));
            return c;
        } catch (SQLException e) {
            throw new Exception("Error buscaCurso()!!", e);
        }
    }
    
    
    @Override
    public int compareTo(Curso o) {
        if (this.getId() == o.getId()) {
            return 0;
        } else if (this.getId() > o.getId()) {
            return 1;
        } else {
            return -1;
        }
    }

}
